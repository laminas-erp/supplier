<?php

namespace Lerp\Supplier;

use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;
use Lerp\Supplier\Controller\Ajax\Autocomplete\SupplierAutocompleteController;
use Lerp\Supplier\Controller\Rest\Address\AddressSupplierRestController;
use Lerp\Supplier\Controller\Rest\Bank\BankSupplierRestController;
use Lerp\Supplier\Controller\Rest\Contact\ContactSupplierRestController;
use Lerp\Supplier\Controller\Rest\Files\FileSupplierRestController;
use Lerp\Supplier\Controller\Rest\SupplierRestController;
use Lerp\Supplier\Controller\Rest\SupplierProductRestController;
use Lerp\Supplier\Controller\Stream\FilestreamController;
use Lerp\Supplier\Factory\Controller\Ajax\Autocomplete\SupplierAutocompleteControllerFactory;
use Lerp\Supplier\Factory\Controller\Rest\Address\AddressSupplierRestControllerFactory;
use Lerp\Supplier\Factory\Controller\Rest\Bank\BankSupplierRestControllerFactory;
use Lerp\Supplier\Factory\Controller\Rest\Contact\ContactSupplierRestControllerFactory;
use Lerp\Supplier\Factory\Controller\Rest\Files\FileSupplierRestControllerFactory;
use Lerp\Supplier\Factory\Controller\Rest\SupplierRestControllerFactory;
use Lerp\Supplier\Factory\Controller\Rest\SupplierProductRestControllerFactory;
use Lerp\Supplier\Factory\Controller\Stream\FilestreamControllerFactory;
use Lerp\Supplier\Factory\Form\Address\AddressSupplierFormFactory;
use Lerp\Supplier\Factory\Form\Bank\BankSupplierFormFactory;
use Lerp\Supplier\Factory\Form\Contact\ContactSupplierFormFactory;
use Lerp\Supplier\Factory\Form\Files\FileSupplierFormFactory;
use Lerp\Supplier\Factory\Form\SupplierFormFactory;
use Lerp\Supplier\Factory\Service\Address\AddressSupplierRelServiceFactory;
use Lerp\Supplier\Factory\Service\Bank\BankSupplierRelServiceFactory;
use Lerp\Supplier\Factory\Service\Contact\ContactSupplierRelServiceFactory;
use Lerp\Supplier\Factory\Service\Files\FileSupplierRelServiceFactory;
use Lerp\Supplier\Factory\Service\SupplierServiceFactory;
use Lerp\Supplier\Factory\Table\Address\AddressSupplierRelTableFactory;
use Lerp\Supplier\Factory\Table\Bank\BankSupplierRelTableFactory;
use Lerp\Supplier\Factory\Table\Contact\ContactSupplierRelTableFactory;
use Lerp\Supplier\Factory\Table\Files\FileSupplierRelTableFactory;
use Lerp\Supplier\Factory\Table\SupplierTableFactory;
use Lerp\Supplier\Factory\Table\ViewSupplierTableFactory;
use Lerp\Supplier\Form\Address\AddressSupplierForm;
use Lerp\Supplier\Form\Bank\BankSupplierForm;
use Lerp\Supplier\Form\Contact\ContactSupplierForm;
use Lerp\Supplier\Form\Files\FileSupplierForm;
use Lerp\Supplier\Form\SupplierForm;
use Lerp\Supplier\Service\Address\AddressSupplierRelService;
use Lerp\Supplier\Service\Bank\BankSupplierRelService;
use Lerp\Supplier\Service\Contact\ContactSupplierRelService;
use Lerp\Supplier\Service\Files\FileSupplierRelService;
use Lerp\Supplier\Service\SupplierService;
use Lerp\Supplier\Table\Address\AddressSupplierRelTable;
use Lerp\Supplier\Table\Bank\BankSupplierRelTable;
use Lerp\Supplier\Table\Contact\ContactSupplierRelTable;
use Lerp\Supplier\Table\Files\FileSupplierRelTable;
use Lerp\Supplier\Table\SupplierTable;
use Lerp\Supplier\Table\ViewSupplierTable;

return [
    'router'             => [
        'routes' => [
            'lerp_supplier_rest_supplier'                    => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-supplier[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => SupplierRestController::class,
                    ],
                ],
            ],
            'lerp_supplier_rest_supplierproduct'             => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-supplier-product[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => SupplierProductRestController::class,
                    ],
                ],
            ],
            'lerp_supplier_rest_addresssupplier'             => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-address-supplier[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => AddressSupplierRestController::class,
                    ],
                ],
            ],
            'lerp_supplier_rest_contactsupplier'             => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-contact-supplier[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => ContactSupplierRestController::class,
                    ],
                ],
            ],
            'lerp_supplier_rest_banksupplier'                => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-bank-supplier[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => BankSupplierRestController::class,
                    ],
                ],
            ],
            'lerp_supplier_rest_filesupplier'                => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-file-supplier[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => FileSupplierRestController::class,
                    ],
                ],
            ],
            /**
             * AJAX - autocomplete
             */
            'lerp_supplier_ajax_autocomplete_suppliernumber' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/lerp-supplier-autocomplete-number',
                    'defaults' => [
                        'controller' => SupplierProductRestController::class,
                        'action'     => 'supplierNumber'
                    ],
                ],
            ],
            /**
             * Stream
             */
            'lerp_supplier_stream_filestream_stream'         => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-supplier-filestream[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => FilestreamController::class,
                        'action'     => 'stream'
                    ],
                ],
            ],
        ],
    ],
    'controllers'        => [
        'factories'  => [
            // REST
            SupplierRestController::class         => SupplierRestControllerFactory::class,
            SupplierProductRestController::class  => SupplierProductRestControllerFactory::class,
            AddressSupplierRestController::class  => AddressSupplierRestControllerFactory::class,
            ContactSupplierRestController::class  => ContactSupplierRestControllerFactory::class,
            BankSupplierRestController::class     => BankSupplierRestControllerFactory::class,
            FileSupplierRestController::class     => FileSupplierRestControllerFactory::class,
            // ajax - autocomplete
            SupplierAutocompleteController::class => SupplierAutocompleteControllerFactory::class,
            // stream
            FilestreamController::class           => FilestreamControllerFactory::class,
        ],
        'invokables' => [],
    ],
    'service_manager'    => [
        'factories'  => [
            // table
            SupplierTable::class             => SupplierTableFactory::class,
            ViewSupplierTable::class         => ViewSupplierTableFactory::class,
            AddressSupplierRelTable::class   => AddressSupplierRelTableFactory::class,
            ContactSupplierRelTable::class   => ContactSupplierRelTableFactory::class,
            BankSupplierRelTable::class      => BankSupplierRelTableFactory::class,
            FileSupplierRelTable::class      => FileSupplierRelTableFactory::class,
            // service
            SupplierService::class           => SupplierServiceFactory::class,
            AddressSupplierRelService::class => AddressSupplierRelServiceFactory::class,
            ContactSupplierRelService::class => ContactSupplierRelServiceFactory::class,
            BankSupplierRelService::class    => BankSupplierRelServiceFactory::class,
            FileSupplierRelService::class    => FileSupplierRelServiceFactory::class,
            // form
            SupplierForm::class              => SupplierFormFactory::class,
            AddressSupplierForm::class       => AddressSupplierFormFactory::class,
            ContactSupplierForm::class       => ContactSupplierFormFactory::class,
            BankSupplierForm::class          => BankSupplierFormFactory::class,
            FileSupplierForm::class          => FileSupplierFormFactory::class,
        ],
        'invokables' => [],
    ],
    'view_helper_config' => [
        'factories'  => [],
        'invokables' => [],
        'aliases'    => [],
    ],
    'view_manager'       => [
        'template_map'        => [],
        'template_path_stack' => [],
        'strategies'          => [
            'ViewJsonStrategy',
        ],
    ],
    'lerp_supplier'      => [
        'module_brand' => 'supplier',
    ],
];

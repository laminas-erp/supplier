<?php

namespace Lerp\Supplier\Entity;

use Bitkorn\Trinket\Entity\ParamsBase;
use Bitkorn\Trinket\Filter\FilterChainStringSanitize;
use Laminas\Db\Sql\Predicate\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Validator\Uuid;

class ParamsSupplier extends ParamsBase
{
    protected array $orderFieldsAvailable = [
        'supplier_no', 'supplier_name', 'supplier_name2', 'supplier_zip', 'supplier_city', 'supplier_lang_iso'
        , 'skr_03_code', 'supplier_time_create', 'country_name', 'country_iso', 'industry_category_label'
    ];
    protected Uuid $uuid;
    protected FilterChainStringSanitize $stringFilter;

    protected int $supplierNo;
    protected string $supplierName;
    protected string $supplierName2;
    protected string $industryCategoryUuid;
    protected string $productUuid;

    public function __construct()
    {
        $this->uuid = new Uuid();
        $this->stringFilter = new FilterChainStringSanitize();
    }

    public function setSupplierNo(int $supplierNo): void
    {
        $this->supplierNo = $supplierNo;
    }

    public function setSupplierName(string $supplierName): void
    {
        $this->supplierName = $this->stringFilter->filter($supplierName);
    }

    public function setSupplierName2(string $supplierName2): void
    {
        $this->supplierName2 = $this->stringFilter->filter($supplierName2);
    }

    public function setIndustryCategoryUuid(string $industryCategoryUuid): void
    {
        if (!$this->uuid->isValid($industryCategoryUuid)) {
            return;
        }
        $this->industryCategoryUuid = $industryCategoryUuid;
    }

    public function setProductUuid(string $productUuid): void
    {
        if (!$this->uuid->isValid($productUuid)) {
            return;
        }
        $this->productUuid = $productUuid;
    }

    public function setFromParamsArray(array $qp): void
    {
        parent::setFromParamsArray($qp);
        $this->setSupplierNo(isset($qp['supplier_no']) ? intval($qp['supplier_no']) : 0);
        $this->setSupplierName($qp['supplier_name'] ?? '');
        $this->setSupplierName2($qp['supplier_name2'] ?? '');
        $this->setIndustryCategoryUuid($qp['industry_category_uuid'] ?? '');
        $this->setProductUuid($qp['product_uuid'] ?? '');
    }

    /**
     * @param Select $select
     * @param string $orderDefault Without ORDER BY, order is undefined and perhaps it loose items during pagination.
     */
    public function computeSelect(Select &$select, string $orderDefault = ''): void
    {
        if (!$this->doCount) {
            parent::computeSelect($select);
        } else {
            $select->columns(['count_supplier' => new Expression('COUNT(*)')]);
        }
        if (!empty($this->supplierNo)) {
            $select->where->like(new Expression('CAST(supplier_no AS TEXT)'), '%' . $this->supplierNo . '%');
        }
        if (!empty($this->supplierName)) {
            $text = strtolower($this->supplierName);
            $select->where->like(new Expression('LOWER(supplier_name)'), '%' . $text . '%');
        }
        if (!empty($this->supplierName2)) {
            $text = strtolower($this->supplierName2);
            $select->where->like(new Expression('LOWER(supplier_name2)'), '%' . $text . '%');
        }
        if (!empty($this->industryCategoryUuid)) {
            $select->where(['industry_category_uuid' => $this->industryCategoryUuid]);
        }
        if (!empty($this->productUuid)) {
            $selectPOItem = new Select('purchase_order_item');
            $selectPOItem->columns(['purchase_order_uuid']);
            $selectPOItem->where(['product_uuid' => $this->productUuid]);
            $selectPO = new Select('purchase_order');
            $selectPO->columns(['supplier_uuid']);
            $selectPO->where->in('purchase_order_uuid', $selectPOItem);

            $selectPRItem = new Select('purchase_request_item');
            $selectPRItem->columns(['purchase_request_uuid']);
            $selectPRItem->where(['product_uuid' => $this->productUuid]);
            $selectPR = new Select('purchase_request');
            $selectPR->columns(['supplier_uuid']);
            $selectPR->where->in('purchase_request_uuid', $selectPRItem);

            $selectProd = new Select('product_supplier');
            $selectProd->columns(['supplier_uuid']);
            $selectProd->where(['product_uuid' => $this->productUuid]);

            $select->where->NEST->in('supplier_uuid', $selectPO)
                ->OR->in('supplier_uuid', $selectPR)
                ->OR->in('supplier_uuid', $selectProd);
        }
    }

}

<?php

namespace Lerp\Supplier\Entity;

use Bitkorn\Trinket\Entity\AbstractEntity;

class SupplierEntity extends AbstractEntity
{
    public array $mapping = [
        'supplier_uuid' => 'supplier_uuid',
        'supplier_no' => 'supplier_no',
        'supplier_name' => 'supplier_name',
        'supplier_name2' => 'supplier_name2',
        'supplier_street' => 'supplier_street',
        'supplier_zip' => 'supplier_zip',
        'supplier_city' => 'supplier_city',
        'country_id' => 'country_id',
        'supplier_tel' => 'supplier_tel',
        'supplier_fax' => 'supplier_fax',
        'supplier_email' => 'supplier_email',
        'supplier_www' => 'supplier_www',
        'supplier_lang_iso' => 'supplier_lang_iso',
        'industry_category_uuid' => 'industry_category_uuid',
        'supplier_tax_id' => 'supplier_tax_id',
        'skr_03_code' => 'skr_03_code',
        'supplier_time_create' => 'supplier_time_create',
        'supplier_time_update' => 'supplier_time_update',
        'supplier_note' => 'supplier_note',
    ];

    protected $primaryKey = 'supplier_uuid';

    public function getSupplierUuid(): string
    {
        if (!isset($this->storage['supplier_uuid'])) {
            return '';
        }
        return $this->storage['supplier_uuid'];
    }

    public function getSupplierNo(): int
    {
        if (!isset($this->storage['supplier_no'])) {
            return 0;
        }
        return $this->storage['supplier_no'];
    }

    public function getSupplierName(): string
    {
        if (!isset($this->storage['supplier_name'])) {
            return '';
        }
        return $this->storage['supplier_name'];
    }

    public function getSupplierName2(): string
    {
        if (!isset($this->storage['supplier_name2'])) {
            return '';
        }
        return $this->storage['supplier_name2'];
    }

    public function getSupplierStreet(): string
    {
        if (!isset($this->storage['supplier_street'])) {
            return '';
        }
        return $this->storage['supplier_street'];
    }

    public function getSupplierZip(): string
    {
        if (!isset($this->storage['supplier_zip'])) {
            return '';
        }
        return $this->storage['supplier_zip'];
    }

    public function getSupplierCity(): string
    {
        if (!isset($this->storage['supplier_city'])) {
            return '';
        }
        return $this->storage['supplier_city'];
    }

    public function getCountryId(): int
    {
        if (!isset($this->storage['country_id'])) {
            return 0;
        }
        return $this->storage['country_id'];
    }

    public function getSupplierTel(): string
    {
        if (!isset($this->storage['supplier_tel'])) {
            return '';
        }
        return $this->storage['supplier_tel'];
    }

    public function getSupplierFax(): string
    {
        if (!isset($this->storage['supplier_fax'])) {
            return '';
        }
        return $this->storage['supplier_fax'];
    }

    public function getSupplierEmail(): string
    {
        if (!isset($this->storage['supplier_email'])) {
            return '';
        }
        return $this->storage['supplier_email'];
    }

    public function getSupplierWww(): string
    {
        if (!isset($this->storage['supplier_www'])) {
            return '';
        }
        return $this->storage['supplier_www'];
    }

    public function getSupplierLangIso(): string
    {
        if (!isset($this->storage['supplier_lang_iso'])) {
            return '';
        }
        return $this->storage['supplier_lang_iso'];
    }

    public function getIndustryCategoryUuid(): string
    {
        if (!isset($this->storage['industry_category_uuid'])) {
            return '';
        }
        return $this->storage['industry_category_uuid'];
    }

    public function getSupplierTaxId(): string
    {
        if (!isset($this->storage['supplier_tax_id'])) {
            return '';
        }
        return $this->storage['supplier_tax_id'];
    }

    public function getSkr03Code(): string
    {
        if (!isset($this->storage['skr_03_code'])) {
            return '';
        }
        return $this->storage['skr_03_code'];
    }

    public function getSupplierTimeCreate(): string
    {
        if (!isset($this->storage['supplier_time_create'])) {
            return '';
        }
        return $this->storage['supplier_time_create'];
    }

    public function getSupplierTimeUpdate(): string
    {
        if (!isset($this->storage['supplier_time_update'])) {
            return '';
        }
        return $this->storage['supplier_time_update'];
    }

    public function getSupplierNote(): string
    {
        if (!isset($this->storage['supplier_note'])) {
            return '';
        }
        return $this->storage['supplier_note'];
    }
}

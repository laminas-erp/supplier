<?php

namespace Lerp\Supplier\Factory\Service\Bank;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Supplier\Service\Bank\BankSupplierRelService;
use Lerp\Supplier\Table\Bank\BankSupplierRelTable;

class BankSupplierRelServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new BankSupplierRelService();
        $service->setLogger($container->get('logger'));
        $service->setBankSupplierRelTable($container->get(BankSupplierRelTable::class));
        return $service;
    }
}

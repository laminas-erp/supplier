<?php

namespace Lerp\Supplier\Factory\Service;

use Interop\Container\ContainerInterface;
use Lerp\Supplier\Service\SupplierService;
use Lerp\Supplier\Table\SupplierTable;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Supplier\Table\ViewSupplierTable;

class SupplierServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new SupplierService();
        $service->setLogger($container->get('logger'));
        $service->setSupplierTable($container->get(SupplierTable::class));
        $service->setViewSupplierTable($container->get(ViewSupplierTable::class));
        return $service;
    }
}

<?php

namespace Lerp\Supplier\Factory\Service\Address;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Supplier\Service\Address\AddressSupplierRelService;
use Lerp\Supplier\Table\Address\AddressSupplierRelTable;

class AddressSupplierRelServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new AddressSupplierRelService();
        $service->setLogger($container->get('logger'));
        $service->setAddressSupplierRelTable($container->get(AddressSupplierRelTable::class));
        return $service;
    }
}

<?php

namespace Lerp\Supplier\Factory\Service\Files;

use Bitkorn\Files\Service\FileService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Supplier\Service\Files\FileSupplierRelService;
use Lerp\Supplier\Table\Files\FileSupplierRelTable;

class FileSupplierRelServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new FileSupplierRelService();
        $service->setLogger($container->get('logger'));
        $service->setFileSupplierRelTable($container->get(FileSupplierRelTable::class));
        $service->setDbAdapter($container->get('dbDefault'));
        $service->setFileService($container->get(FileService::class));
        return $service;
    }
}

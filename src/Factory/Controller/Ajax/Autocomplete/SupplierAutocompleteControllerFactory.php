<?php

namespace Lerp\Supplier\Factory\Controller\Ajax\Autocomplete;

use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Lerp\Supplier\Controller\Ajax\Autocomplete\SupplierAutocompleteController;
use Lerp\Supplier\Service\SupplierService;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class SupplierAutocompleteControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new SupplierAutocompleteController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setSupplierService($container->get(SupplierService::class));
        return $controller;
    }
}

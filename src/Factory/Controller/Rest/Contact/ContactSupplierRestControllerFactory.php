<?php

namespace Lerp\Supplier\Factory\Controller\Rest\Contact;

use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Supplier\Controller\Rest\Contact\ContactSupplierRestController;
use Lerp\Supplier\Form\Contact\ContactSupplierForm;
use Lerp\Supplier\Service\Contact\ContactSupplierRelService;

class ContactSupplierRestControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new ContactSupplierRestController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setContactSupplierForm($container->get(ContactSupplierForm::class));
        $controller->setContactSupplierRelService($container->get(ContactSupplierRelService::class));
        return $controller;
    }
}

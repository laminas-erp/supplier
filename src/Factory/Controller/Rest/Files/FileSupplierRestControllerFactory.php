<?php

namespace Lerp\Supplier\Factory\Controller\Rest\Files;

use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Supplier\Controller\Rest\Files\FileSupplierRestController;
use Lerp\Supplier\Form\Files\FileSupplierForm;
use Lerp\Supplier\Service\Files\FileSupplierRelService;

class FileSupplierRestControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new FileSupplierRestController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setFileSupplierForm($container->get(FileSupplierForm::class));
        $controller->setFileSupplierRelService($container->get(FileSupplierRelService::class));
        $controller->setModuleBrand($container->get('config')['lerp_supplier']['module_brand']);
        return $controller;
    }
}

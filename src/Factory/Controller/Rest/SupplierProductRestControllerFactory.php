<?php

namespace Lerp\Supplier\Factory\Controller\Rest;

use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Lerp\Supplier\Controller\Rest\SupplierProductRestController;
use Lerp\Supplier\Service\SupplierService;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class SupplierProductRestControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new SupplierProductRestController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setSupplierService($container->get(SupplierService::class));
        return $controller;
    }
}

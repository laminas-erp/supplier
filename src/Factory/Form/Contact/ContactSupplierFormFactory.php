<?php

namespace Lerp\Supplier\Factory\Form\Contact;

use Bitkorn\Contact\Form\Fieldset\ContactFieldset;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Supplier\Form\Contact\ContactSupplierForm;

class ContactSupplierFormFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $form = new ContactSupplierForm();
        $form->setContactFieldset($container->get(ContactFieldset::class));
        return $form;
    }
}

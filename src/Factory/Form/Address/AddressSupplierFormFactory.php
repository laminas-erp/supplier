<?php

namespace Lerp\Supplier\Factory\Form\Address;

use Bitkorn\Address\Form\Fieldset\AddressFieldset;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Supplier\Form\Address\AddressSupplierForm;

class AddressSupplierFormFactory implements FactoryInterface
{
	/**
	 * Create an object
	 *
	 * @param ContainerInterface $container
	 * @param string $requestedName
	 * @param null|array $options
	 * @return object
	 * @throws ServiceNotFoundException if unable to resolve the service
	 * @throws ServiceNotCreatedException if an exception is raised when creating a service
	 */
	public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
	{
		$form = new AddressSupplierForm();
        $form->setAddressFieldset($container->get(AddressFieldset::class));
		return $form;
	}
}

<?php

namespace Lerp\Supplier\Factory\Table\Address;

use Bitkorn\Address\Table\AddressTable;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Supplier\Table\Address\AddressSupplierRelTable;

class AddressSupplierRelTableFactory implements FactoryInterface
{
	/**
	 * Create an object
	 *
	 * @param ContainerInterface $container
	 * @param string $requestedName
	 * @param null|array $options
	 * @return object
	 * @throws ServiceNotFoundException if unable to resolve the service
	 * @throws ServiceNotCreatedException if an exception is raised when creating a service
	 */
	public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
	{
		$table = new AddressSupplierRelTable();
		$table->setLogger($container->get('logger'));
		$table->setDbAdapter($container->get('dbDefault'));
        $table->setAddressTable($container->get(AddressTable::class));
		return $table;
	}
}

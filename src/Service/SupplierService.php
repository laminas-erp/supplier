<?php

namespace Lerp\Supplier\Service;

use Bitkorn\Trinket\Service\AbstractService;
use Lerp\Supplier\Entity\ParamsSupplier;
use Lerp\Supplier\Entity\SupplierEntity;
use Lerp\Supplier\Table\SupplierTable;
use Lerp\Supplier\Table\ViewSupplierTable;

class SupplierService extends AbstractService
{
    protected SupplierTable $supplierTable;
    protected ViewSupplierTable $viewSupplierTable;
    protected int $searchSupplierCount = 0;

    public function setSupplierTable(SupplierTable $supplierTable): void
    {
        $this->supplierTable = $supplierTable;
    }

    public function setViewSupplierTable(ViewSupplierTable $viewSupplierTable): void
    {
        $this->viewSupplierTable = $viewSupplierTable;
    }

    /**
     * @param string $supplierUuid
     * @return array FROM view_supplier
     */
    public function getSupplier(string $supplierUuid): array
    {
        return $this->viewSupplierTable->getSupplier($supplierUuid);
    }

    /**
     * @param string $productUuid
     * @return array FROM view_supplier
     */
    public function getSuppliersForProductAll(string $productUuid): array
    {
        return $this->viewSupplierTable->getSuppliersForProductAll($productUuid);
    }

    public function insertSupplier(SupplierEntity $supplierEntity): string
    {
        return $this->supplierTable->insertSupplier($supplierEntity);
    }

    public function updateSupplier(string $supplierUuid, SupplierEntity $supplierEntity): bool
    {
        return $this->supplierTable->updateSupplier($supplierUuid, $supplierEntity) >= 0;
    }

    /**
     * @param ParamsSupplier $paramsSupplier
     * @return array
     */
    public function searchSupplier(ParamsSupplier $paramsSupplier): array
    {
        $paramsSupplier->setDoCount(true);
        $this->searchSupplierCount = $this->supplierTable->searchSupplier($paramsSupplier)['count_supplier'];
        $paramsSupplier->setDoCount(false);
        return $this->supplierTable->searchSupplier($paramsSupplier);
    }

    /**
     * @return int
     */
    public function getSearchSupplierCount(): int
    {
        return $this->searchSupplierCount;
    }

    /**
     * @param int $supplierNo
     * @return array
     */
    public function getSuppliersLikeSupplierNo(int $supplierNo): array
    {
        return $this->supplierTable->getSuppliersLikeSupplierNo($supplierNo);
    }

}

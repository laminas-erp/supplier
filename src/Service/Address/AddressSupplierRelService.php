<?php

namespace Lerp\Supplier\Service\Address;

use Bitkorn\Address\Entity\AddressEntity;
use Bitkorn\Trinket\Service\AbstractService;
use Lerp\Supplier\Table\Address\AddressSupplierRelTable;

class AddressSupplierRelService extends AbstractService
{
    protected AddressSupplierRelTable $addressSupplierRelTable;

    public function setAddressSupplierRelTable(AddressSupplierRelTable $addressSupplierRelTable): void
    {
        $this->addressSupplierRelTable = $addressSupplierRelTable;
    }

    public function getAddressSupplierRelJoined(string $addressSupplierRelUuid): array
    {
        return $this->addressSupplierRelTable->getAddressSupplierRelJoined($addressSupplierRelUuid);
    }

    public function insertAddress(AddressEntity $addressEntity, string $supplierUuid): string
    {
        return $this->addressSupplierRelTable->insertAddress($addressEntity, $supplierUuid);
    }

    public function deleteAddress(string $addressSupplierRelUuid): bool
    {
        return $this->addressSupplierRelTable->deleteAddress($addressSupplierRelUuid);
    }

    public function getAddressesForSupplier(string $supplierUuid): array
    {
        return $this->addressSupplierRelTable->getAddressesForSupplier($supplierUuid);
    }

    public function updateAddress(AddressEntity $addressEntity, string $supplierUuid): bool
    {
        return $this->addressSupplierRelTable->updateAddress($addressEntity, $supplierUuid) >= 0;
    }
}

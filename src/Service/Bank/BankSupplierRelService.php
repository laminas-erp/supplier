<?php

namespace Lerp\Supplier\Service\Bank;

use Bitkorn\Bank\Entity\BankEntity;
use Bitkorn\Trinket\Service\AbstractService;
use Lerp\Supplier\Table\Bank\BankSupplierRelTable;

class BankSupplierRelService extends AbstractService
{
    protected BankSupplierRelTable $bankSupplierRelTable;

    public function setBankSupplierRelTable(BankSupplierRelTable $bankSupplierRelTable): void
    {
        $this->bankSupplierRelTable = $bankSupplierRelTable;
    }

    public function getBankSupplierRelJoined(string $bankSupplierRelUuid): array
    {
        return $this->bankSupplierRelTable->getBankSupplierRelJoined($bankSupplierRelUuid);
    }

    public function insertBank(BankEntity $bankEntity, string $supplierUuid): string
    {
        return $this->bankSupplierRelTable->insertBank($bankEntity, $supplierUuid);
    }

    public function deleteBank(string $bankSupplierRelUuid): bool
    {
        return $this->bankSupplierRelTable->deleteBank($bankSupplierRelUuid);
    }

    public function getBanksForSupplier(string $supplierUuid): array
    {
        return $this->bankSupplierRelTable->getBanksForSupplier($supplierUuid);
    }

    public function updateBank(BankEntity $bankEntity, string $supplierUuid): bool
    {
        $bankEntity->unsetDbUnusedFields();
        return $this->bankSupplierRelTable->updateBank($bankEntity, $supplierUuid) >= 0;
    }

}

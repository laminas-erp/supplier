<?php

namespace Lerp\Supplier\Service\Files;

use Bitkorn\Files\Entity\FileEntity;
use Bitkorn\Files\Service\FileService;
use Bitkorn\Trinket\Service\AbstractService;
use Laminas\Db\Adapter\Adapter;
use Laminas\Db\Adapter\AdapterAwareInterface;
use Laminas\Db\Adapter\Driver\AbstractConnection;
use Laminas\Log\Logger;
use Lerp\Supplier\Table\Files\FileSupplierRelTable;

class FileSupplierRelService extends AbstractService implements AdapterAwareInterface
{
    protected FileSupplierRelTable $fileSupplierRelTable;
    protected Adapter $adapter;
    protected FileService $fileService;

    public function setFileSupplierRelTable(FileSupplierRelTable $fileSupplierRelTable): void
    {
        $this->fileSupplierRelTable = $fileSupplierRelTable;
    }

    public function setDbAdapter(Adapter $adapter)
    {
        $this->adapter = $adapter;
    }

    public function setFileService(FileService $fileService): void
    {
        $this->fileService = $fileService;
    }

    /**
     * @param string $fileSupplierRelUuid
     * @return array
     */
    public function getFileSupplierRel(string $fileSupplierRelUuid): array
    {
        return $this->fileSupplierRelTable->getFileSupplierRel($fileSupplierRelUuid);
    }

    public function getFileSupplierRelJoined(string $fileSupplierRelUuid): array
    {
        return $this->fileSupplierRelTable->getFileSupplierRelJoined($fileSupplierRelUuid);
    }

    /**
     * @param FileEntity $fileEntity
     * @param string $supplierUuid
     * @param string $folderBrand
     * @return string fileSupplierRelUuid
     */
    public function handleFileUpload(FileEntity $fileEntity, string $supplierUuid, string $folderBrand = ''): string
    {
        if (empty($fileUuid = $this->fileService->handleFileUpload($fileEntity, $folderBrand))) {
            return '';
        }

        if (empty($fileSupplierRelUuid = $this->fileSupplierRelTable->insertFileSupplierRel($fileUuid, $supplierUuid))) {
            $this->fileService->deleteFile($fileUuid);
            return '';
        }
        return $fileSupplierRelUuid;
    }

    public function deleteFile(string $fileSupplierRelUuid): bool
    {
        $connection = $this->beginTransactionAdapter($this->adapter);

        $fileRel = $this->fileSupplierRelTable->getFileSupplierRel($fileSupplierRelUuid);
        if (
            empty($fileRel) || !isset($fileRel['file_uuid'])
            || $this->fileSupplierRelTable->deleteFile($fileSupplierRelUuid) < 0
            || !$this->fileService->deleteFile($fileRel['file_uuid'])
        ) {
            $connection->rollback();
            return false;
        }
        $connection->commit();
        return true;
    }

    public function getFilesForSupplier(string $supplierUuid): array
    {
        return $this->fileSupplierRelTable->getFilesForSupplier($supplierUuid);
    }

    public function updateFile(FileEntity $fileEntity, string $supplierUuid): bool
    {
        return $this->fileSupplierRelTable->updateFile($fileEntity, $supplierUuid) >= 0;
    }
}

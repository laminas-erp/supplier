<?php

namespace Lerp\Supplier\Service\Contact;

use Bitkorn\Contact\Entity\ContactEntity;
use Bitkorn\Trinket\Service\AbstractService;
use Laminas\Log\Logger;
use Lerp\Supplier\Table\Contact\ContactSupplierRelTable;

class ContactSupplierRelService extends AbstractService
{
    protected ContactSupplierRelTable $contactSupplierRelTable;

    public function setContactSupplierRelTable(ContactSupplierRelTable $contactSupplierRelTable): void
    {
        $this->contactSupplierRelTable = $contactSupplierRelTable;
    }

    public function getContactSupplierRelJoined(string $contactSupplierRelUuid): array
    {
        return $this->contactSupplierRelTable->getContactSupplierRelJoined($contactSupplierRelUuid);
    }

    public function insertContact(ContactEntity $contactEntity, string $supplierUuid): string
    {
        return $this->contactSupplierRelTable->insertContact($contactEntity, $supplierUuid);
    }

    public function deleteContact(string $contactSupplierRelUuid): bool
    {
        return $this->contactSupplierRelTable->deleteContact($contactSupplierRelUuid);
    }

    public function getContactsForSupplier(string $supplierUuid): array
    {
        return $this->contactSupplierRelTable->getContactsForSupplier($supplierUuid);
    }

    public function updateContact(ContactEntity $contactEntity, string $supplierUuid): bool
    {
        return $this->contactSupplierRelTable->updateContact($contactEntity, $supplierUuid) >= 0;
    }

}

<?php

namespace Lerp\Supplier\Controller\Rest;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Laminas\Validator\Uuid;
use Lerp\Supplier\Service\SupplierService;
use Laminas\Http\Response;

class SupplierProductRestController extends AbstractUserRestController
{
    protected SupplierService $supplierService;

    public function setSupplierService(SupplierService $supplierService): void
    {
        $this->supplierService = $supplierService;
    }

    /**
     * GET suppliers for product_uuid
     *
     * @param string $id product_uuid
     * @return JsonModel
     */
    public function get($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setArr($this->supplierService->getSuppliersForProductAll($id));
        return $jsonModel;
    }
}

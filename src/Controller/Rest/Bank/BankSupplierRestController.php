<?php

namespace Lerp\Supplier\Controller\Rest\Bank;

use Bitkorn\Bank\Entity\BankEntity;
use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;
use Lerp\Supplier\Form\Bank\BankSupplierForm;
use Lerp\Supplier\Service\Bank\BankSupplierRelService;

class BankSupplierRestController extends AbstractUserRestController
{
    protected BankSupplierForm $bankSupplierForm;
    protected BankSupplierRelService $bankSupplierRelService;

    public function setBankSupplierForm(BankSupplierForm $bankSupplierForm): void
    {
        $this->bankSupplierForm = $bankSupplierForm;
    }

    public function setBankSupplierRelService(BankSupplierRelService $bankSupplierRelService): void
    {
        $this->bankSupplierRelService = $bankSupplierRelService;
    }

    /**
     * POST maps to create().
     * @param array $data
     * @return JsonModel
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $this->bankSupplierForm->getBankFieldset()->setPrimaryKeyAvailable(false)->init();
        $this->bankSupplierForm->setData($data);
        $supplierUuid = $data['supplier_uuid'];
        $bankEntity = new BankEntity();
        if (!(new Uuid())->isValid($supplierUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!$this->bankSupplierForm->isValid()) {
            $jsonModel->addMessages($this->bankSupplierForm->getMessages());
            return $jsonModel;
        }
        if (!$bankEntity->exchangeArrayFromDatabase($this->bankSupplierForm->getData())) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        if (!empty($uuid = $this->bankSupplierRelService->insertBank($bankEntity, $supplierUuid))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_201);
            $jsonModel->setSuccess(1);
            $jsonModel->setUuid($uuid);
        }
        return $jsonModel;
    }

    /**
     * DELETE maps to delete().
     * @param string $id bank_supplier_rel_uuid
     * @return JsonModel
     */
    public function delete($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if ($this->bankSupplierRelService->deleteBank($id)) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * GET
     * @return JsonModel
     */
    public function getList(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $supplierUuid = filter_input(INPUT_GET, 'supplier_uuid', FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        if ((new Uuid())->isValid($supplierUuid) && !empty($suppliers = $this->bankSupplierRelService->getBanksForSupplier($supplierUuid))) {
            $jsonModel->setArr($suppliers);
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * PUT maps to update().
     * @param string $id supplier_uuid
     * @param array $data
     * @return JsonModel
     */
    public function update($id, $data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $data['supplier_uuid'] = $id;
        $this->bankSupplierForm->setData($data);
        $bankEntity = new BankEntity();
        if (!$this->bankSupplierForm->isValid()) {
            $jsonModel->addMessages($this->bankSupplierForm->getMessages());
            return $jsonModel;
        }
        if (!$bankEntity->exchangeArrayFromDatabase($this->bankSupplierForm->getData())) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        if ($this->bankSupplierRelService->updateBank($bankEntity, $id)) {
            $jsonModel->setSuccess(1);
        } else {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
        }
        return $jsonModel;
    }

    /**
     * GET
     * @param string $id bank_supplier_rel_uuid
     * @return JsonModel
     */
    public function get($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!empty($address = $this->bankSupplierRelService->getBankSupplierRelJoined($id))) {
            $jsonModel->setObj($address);
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }
}

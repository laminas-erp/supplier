<?php

namespace Lerp\Supplier\Controller\Rest\Contact;

use Bitkorn\Contact\Entity\ContactEntity;
use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;
use Lerp\Supplier\Form\Contact\ContactSupplierForm;
use Lerp\Supplier\Service\Contact\ContactSupplierRelService;

class ContactSupplierRestController extends AbstractUserRestController
{
    protected ContactSupplierForm $contactSupplierForm;
    protected ContactSupplierRelService $contactSupplierRelService;

    public function setContactSupplierForm(ContactSupplierForm $contactSupplierForm): void
    {
        $this->contactSupplierForm = $contactSupplierForm;
    }

    public function setContactSupplierRelService(ContactSupplierRelService $contactSupplierRelService): void
    {
        $this->contactSupplierRelService = $contactSupplierRelService;
    }

    /**
     * POST maps to create().
     * @param array $data
     * @return JsonModel
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $this->contactSupplierForm->getContactFieldset()->setPrimaryKeyAvailable(false)->init();
        $this->contactSupplierForm->setData($data);
        $supplierUuid = $data['supplier_uuid'];
        $contactEntity = new ContactEntity();
        if (!(new Uuid())->isValid($supplierUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!$this->contactSupplierForm->isValid()) {
            $jsonModel->addMessages($this->contactSupplierForm->getMessages());
            return $jsonModel;
        }
        if (!$contactEntity->exchangeArrayFromDatabase($this->contactSupplierForm->getData())) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        if (!empty($uuid = $this->contactSupplierRelService->insertContact($contactEntity, $supplierUuid))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_201);
            $jsonModel->setSuccess(1);
            $jsonModel->setUuid($uuid);
        }
        return $jsonModel;
    }

    /**
     * DELETE maps to delete().
     * @param string $id contact_supplier_rel_uuid
     * @return JsonModel
     */
    public function delete($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if ($this->contactSupplierRelService->deleteContact($id)) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * GET
     * @return JsonModel
     */
    public function getList(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $supplierUuid = filter_input(INPUT_GET, 'supplier_uuid', FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        if (!(new Uuid())->isValid($supplierUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            $jsonModel->setSuccess(1);
        }
        if (!empty($suppliers = $this->contactSupplierRelService->getContactsForSupplier($supplierUuid))) {
            $jsonModel->setArr($suppliers);
        }
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * PUT maps to update().
     * @param string $id
     * @param array $data
     * @return JsonModel
     */
    public function update($id, $data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $data['supplier_uuid'] = $id;
        $this->contactSupplierForm->setData($data);
        $contactEntity = new ContactEntity();
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!$this->contactSupplierForm->isValid()) {
            $jsonModel->addMessages($this->contactSupplierForm->getMessages());
            return $jsonModel;
        }
        if (!$contactEntity->exchangeArrayFromDatabase($this->contactSupplierForm->getData())) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        if ($this->contactSupplierRelService->updateContact($contactEntity, $id)) {
            $jsonModel->setSuccess(1);
        } else {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
        }
        return $jsonModel;
    }

    /**
     * GET
     * @param string $id contact_supplier_rel_uuid
     * @return JsonModel
     */
    public function get($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!empty($contact = $this->contactSupplierRelService->getContactSupplierRelJoined($id))) {
            $jsonModel->setObj($contact);
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }
}

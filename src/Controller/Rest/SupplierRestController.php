<?php

namespace Lerp\Supplier\Controller\Rest;

use Bitkorn\Trinket\Filter\FilterChainStringSanitize;
use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Lerp\Supplier\Entity\ParamsSupplier;
use Lerp\Supplier\Entity\SupplierEntity;
use Lerp\Supplier\Form\SupplierForm;
use Lerp\Supplier\Service\SupplierService;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;

class SupplierRestController extends AbstractUserRestController
{
    protected SupplierForm $supplierForm;
    protected SupplierService $supplierService;

    public function setSupplierForm(SupplierForm $supplierForm): void
    {
        $this->supplierForm = $supplierForm;
    }

    public function setSupplierService(SupplierService $supplierService): void
    {
        $this->supplierService = $supplierService;
    }

    /**
     * POST maps to create().
     * @param array $data
     * @return JsonModel
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $this->supplierForm->init();
        $this->supplierForm->setData($data);
        if (!$this->supplierForm->isValid()) {
            $jsonModel->addMessages($this->supplierForm->getMessages());
            return $jsonModel;
        }
        $supplierEntity = new SupplierEntity();
        if (!$supplierEntity->exchangeArrayFromDatabase($this->supplierForm->getData()) || empty($supplierUuid = $this->supplierService->insertSupplier($supplierEntity))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        $jsonModel->setSuccess(1);
        $jsonModel->setUuid($supplierUuid);
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_201);
        return $jsonModel;
    }

    /**
     * PUT maps to update().
     * @param string $id
     * @param array $data
     * @return JsonModel
     */
    public function update($id, $data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $this->supplierForm->init();
        $this->supplierForm->setData($data);
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!$this->supplierForm->isValid()) {
            $jsonModel->addMessages($this->supplierForm->getMessages());
            return $jsonModel;
        }
        $supplierEntity = new SupplierEntity('supplier_uuid');
        if (!$supplierEntity->exchangeArrayFromDatabase($this->supplierForm->getData()) || !$this->supplierService->updateSupplier($id, $supplierEntity)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * GET
     * @param string $id
     * @return JsonModel
     */
    public function get($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setObj($this->supplierService->getSupplier($id));
        return $jsonModel;
    }

    /**
     * GET supplier search
     *
     * @return JsonModel
     */
    public function getList(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        /** @var Request $request */
        $request = $this->getRequest();
        $paramsSupplier = new ParamsSupplier();
        $paramsSupplier->setFromParamsArray($request->getQuery()->toArray());
        $jsonModel->setArr($this->supplierService->searchSupplier($paramsSupplier));
        $jsonModel->setCount($this->supplierService->getSearchSupplierCount());
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * DELETE maps to delete().
     * @param string $id
     * @return JsonModel
     */
    public function delete($id): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }

}

<?php

namespace Lerp\Supplier\Controller\Rest\Address;

use Bitkorn\Address\Entity\AddressEntity;
use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;
use Lerp\Supplier\Form\Address\AddressSupplierForm;
use Lerp\Supplier\Service\Address\AddressSupplierRelService;

class AddressSupplierRestController extends AbstractUserRestController
{
    protected AddressSupplierRelService $addressSupplierRelService;
    protected AddressSupplierForm $addressSupplierForm;

    public function setAddressSupplierRelService(AddressSupplierRelService $addressSupplierRelService): void
    {
        $this->addressSupplierRelService = $addressSupplierRelService;
    }

    public function setAddressSupplierForm(AddressSupplierForm $addressSupplierForm): void
    {
        $this->addressSupplierForm = $addressSupplierForm;
    }

    /**
     * POST maps to create().
     * @param array $data
     * @return JsonModel
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $this->addressSupplierForm->getAddressFieldset()->setPrimaryKeyAvailable(false)->init();
        $this->addressSupplierForm->setData($data);
        $supplierUuid = $data['supplier_uuid'];
        $addressEntity = new AddressEntity();
        if (!(new Uuid())->isValid($supplierUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!$this->addressSupplierForm->isValid()) {
            $jsonModel->addMessages($this->addressSupplierForm->getMessages());
            return $jsonModel;
        }
        if (!$addressEntity->exchangeArrayFromDatabase($this->addressSupplierForm->getData())) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        if (!empty($uuid = $this->addressSupplierRelService->insertAddress($addressEntity, $supplierUuid))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_201);
            $jsonModel->setSuccess(1);
            $jsonModel->setUuid($uuid);
        }
        return $jsonModel;
    }

    /**
     * DELETE maps to delete().
     * @param string $id address_supplier_rel_uuid
     * @return JsonModel
     */
    public function delete($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if ($this->addressSupplierRelService->deleteAddress($id)) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * GET
     * @return JsonModel
     */
    public function getList(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $supplierUuid = filter_input(INPUT_GET, 'supplier_uuid', FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        if ((new Uuid())->isValid($supplierUuid) && !empty($suppliers = $this->addressSupplierRelService->getAddressesForSupplier($supplierUuid))) {
            $jsonModel->setArr($suppliers);
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * PUT maps to update().
     * @param string $id supplier UUID
     * @param array $data
     * @return JsonModel
     */
    public function update($id, $data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $data['supplier_uuid'] = $id;
        $this->addressSupplierForm->setData($data);
        $addressEntity = new AddressEntity();
        if (!$this->addressSupplierForm->isValid()) {
            $jsonModel->addMessages($this->addressSupplierForm->getMessages());
            return $jsonModel;
        }
        if (!$addressEntity->exchangeArrayFromDatabase($this->addressSupplierForm->getData())) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        if ($this->addressSupplierRelService->updateAddress($addressEntity, $id)) {
            $jsonModel->setSuccess(1);
        } else {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
        }
        return $jsonModel;
    }

    /**
     * GET
     * @param string $id address_supplier_rel_uuid
     * @return JsonModel
     */
    public function get($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!empty($address = $this->addressSupplierRelService->getAddressSupplierRelJoined($id))) {
            $jsonModel->setObj($address);
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }
}

<?php

namespace Lerp\Supplier\Controller\Ajax\Autocomplete;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Lerp\Supplier\Service\SupplierService;
use Laminas\Http\Response;

class SupplierAutocompleteController extends AbstractUserController
{
    protected SupplierService $supplierService;

    public function setSupplierService(SupplierService $supplierService): void
    {
        $this->supplierService = $supplierService;
    }

    /**
     * @return JsonModel
     */
    public function supplierNumberAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }

        $supplierNo = intval($this->params('supplier_no'));
        if (empty($supplierNo)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }

        $suppliers = $this->supplierService->getSuppliersLikeSupplierNo($supplierNo);
        if (empty($suppliers)) {
            return $jsonModel;
        }

        $termResult = [];
        foreach ($suppliers as $supplier) {
            $obj = new \stdClass();
            $obj->label = $supplier['supplier_no'] . ' - ' . $supplier['supplier_name'];
            $obj->value = $supplier['supplier_uuid'];
            $termResult[] = $obj;
        }
        $jsonModel->setVariables($termResult);
        return $jsonModel;
    }
}

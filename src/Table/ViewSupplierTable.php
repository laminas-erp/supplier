<?php

namespace Lerp\Supplier\Table;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\Adapter\ParameterContainer;
use Laminas\Db\ResultSet\HydratingResultSet;

class ViewSupplierTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'view_supplier';

    /**
     * @param string $supplierUuid
     * @return array
     */
    public function getSupplier(string $supplierUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['supplier_uuid' => $supplierUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $productUuid
     * @return array
     */
    public function getSuppliersForProductAll(string $productUuid): array
    {
        $params = new ParameterContainer(['product_uuid' => $productUuid]);
        $stmt = $this->adapter->createStatement('SELECT * FROM lerp_query_suppliers_for_product_all(:product_uuid)', $params);
        $result = $stmt->execute();
        $suppliers = [];
        if (!$result->valid() || $result->count() < 1) {
            return [];
        }
        do {
            $suppliers[] = $result->current();
            $result->next();
        } while ($result->valid());
        return $suppliers;
    }
}

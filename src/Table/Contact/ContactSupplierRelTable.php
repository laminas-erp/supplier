<?php

namespace Lerp\Supplier\Table\Contact;

use Bitkorn\Contact\Entity\ContactEntity;
use Bitkorn\Contact\Table\AbstractContactTable;
use Laminas\Db\Adapter\Driver\AbstractConnection;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class ContactSupplierRelTable extends AbstractContactTable
{
    /** @var string */
    protected $table = 'contact_supplier_rel';

    /**
     * @param string $contactSupplierRelUuid
     * @return array
     */
    public function getContactSupplierRel(string $contactSupplierRelUuid)
    {
        $select = $this->sql->select();
        try {
            $select->where(['contact_supplier_rel_uuid' => $contactSupplierRelUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $contactSupplierRelUuid
     * @return array
     */
    public function getContactSupplierRelJoined(string $contactSupplierRelUuid)
    {
        $select = $this->sql->select();
        try {
            $select->join('contact', 'contact.contact_uuid = contact_supplier_rel.contact_uuid'
                , $this->contactColumns
                , Select::JOIN_LEFT);
            $select->where(['contact_supplier_rel_uuid' => $contactSupplierRelUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getContactSupplierRelForContactAndSupplier(string $contactUuid, string $supplierUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['contact_uuid' => $contactUuid, 'supplier_uuid' => $supplierUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function existContactSupplierRelForContactAndSupplier(string $contactUuid, string $supplierUuid): bool
    {
        $contact = $this->getContactSupplierRelForContactAndSupplier($contactUuid, $supplierUuid);
        return !empty($contact) && is_array($contact);
    }

    public function getContactsForSupplier(string $supplierUuid)
    {
        $select = $this->sql->select();
        try {
            $select->join('contact', 'contact.contact_uuid = contact_supplier_rel.contact_uuid'
                , $this->contactColumns
                , Select::JOIN_LEFT);
            $select->where(['supplier_uuid' => $supplierUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    protected function insertContactSupplierRel(string $contactUuid, string $supplierUuid): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'contact_supplier_rel_uuid' => $uuid,
                'contact_uuid' => $contactUuid,
                'supplier_uuid' => $supplierUuid,
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function insertContact(ContactEntity $contactEntity, string $supplierUuid): string
    {
        /** @var AbstractConnection $connection */
        $connection = $this->adapter->getDriver()->getConnection();
        $connection->beginTransaction();

        if (
            empty($contactUuid = $this->contactTable->insertContact($contactEntity))
            || empty($contactSupplierRelUuid = $this->insertContactSupplierRel($contactUuid, $supplierUuid))
        ) {
            $connection->rollback();
            return '';
        }
        $connection->commit();
        return $contactSupplierRelUuid;
    }

    public function deleteContact(string $contactSupplierRelUuid): bool
    {
        $contactrel = $this->getContactSupplierRel($contactSupplierRelUuid);
        if (empty($contactrel) || !is_array($contactrel)) {
            return false;
        }

        /** @var AbstractConnection $connection */
        $connection = $this->adapter->getDriver()->getConnection();
        $connection->beginTransaction();

        $delete = $this->sql->delete();
        try {
            $delete->where(['contact_supplier_rel_uuid' => $contactSupplierRelUuid]);
            if ($this->executeDelete($delete) < 1) {
                $connection->rollback();
                return false;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }

        if ($this->contactTable->deleteContact($contactrel['contact_uuid']) < 1) {
            $connection->rollback();
            return false;
        }

        $connection->commit();
        return true;
    }

    public function updateContact(ContactEntity $contactEntity, string $supplierUuid): int
    {
        $contactUuid = $contactEntity->getContactUuid();
        if (!$this->existContactSupplierRelForContactAndSupplier($contactUuid, $supplierUuid)) {
            return -1;
        }
        $contactEntity->unsetPrimaryKey();
        return $this->contactTable->updateContact($contactUuid, $contactEntity);
    }
}

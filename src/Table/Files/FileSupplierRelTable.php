<?php

namespace Lerp\Supplier\Table\Files;

use Bitkorn\Files\Entity\FileEntity;
use Bitkorn\Files\Table\AbstractFileTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class FileSupplierRelTable extends AbstractFileTable
{
    /** @var string */
    protected $table = 'file_supplier_rel';

    /**
     * @param string $fileSupplierRelUuid
     * @return array
     */
    public function getFileSupplierRel(string $fileSupplierRelUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['file_supplier_rel_uuid' => $fileSupplierRelUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function existFileSupplierRel(string $fileSupplierRelUuid): bool
    {
        return !empty($this->getFileSupplierRel($fileSupplierRelUuid));
    }

    /**
     * @param string $fileSupplierRelUuid
     * @return array
     */
    public function getFileSupplierRelJoined(string $fileSupplierRelUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->join('file', 'file.file_uuid = file_supplier_rel.file_uuid'
                , $this->filesColumns
                , Select::JOIN_LEFT);
            $select->where(['file_supplier_rel_uuid' => $fileSupplierRelUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getFileSupplierRelForFileAndSupplier(string $fileUuid, string $supplierUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['file_uuid' => $fileUuid, 'supplier_uuid' => $supplierUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function existFileSupplierRelForFileAndSupplier(string $fileUuid, string $supplierUuid): bool
    {
        $file = $this->getFileSupplierRelForFileAndSupplier($fileUuid, $supplierUuid);
        return !empty($file) && is_array($file);
    }

    public function getFilesForSupplier(string $supplierUuid)
    {
        $select = $this->sql->select();
        try {
            $select->join('file', 'file.file_uuid = file_supplier_rel.file_uuid'
                , $this->filesColumns
                , Select::JOIN_LEFT);
            $select->where(['supplier_uuid' => $supplierUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function insertFileSupplierRel(string $fileUuid, string $supplierUuid): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'file_supplier_rel_uuid' => $uuid,
                'file_uuid' => $fileUuid,
                'supplier_uuid' => $supplierUuid,
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function deleteFile(string $fileSupplierRelUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['file_supplier_rel_uuid' => $fileSupplierRelUuid]);
            return $this->executeDelete($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function updateFile(FileEntity $fileEntity, string $supplierUuid): int
    {
        $fileUuid = $fileEntity->getFileUuid();
        if (!$this->existFileSupplierRelForFileAndSupplier($fileUuid, $supplierUuid)) {
            return -1;
        }
        $fileEntity->unsetPrimaryKey();
        return $this->fileTable->updateFile($fileUuid, $fileEntity);
    }
}

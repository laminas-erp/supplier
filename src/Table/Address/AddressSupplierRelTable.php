<?php

namespace Lerp\Supplier\Table\Address;

use Bitkorn\Address\Entity\AddressEntity;
use Bitkorn\Address\Table\AbstractAddressTable;
use Laminas\Db\Adapter\Driver\AbstractConnection;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class AddressSupplierRelTable extends AbstractAddressTable
{
    /** @var string */
    protected $table = 'address_supplier_rel';

    public function getAddressSupplierRel(string $addressSupplierRelUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['address_supplier_rel_uuid' => $addressSupplierRelUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getAddressSupplierRelJoined(string $addressSupplierRelUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->join('address', 'address.address_uuid = address_supplier_rel.address_uuid'
                , $this->addressColumns
                , Select::JOIN_LEFT);
            $select->join('country', 'country.country_id = address.country_id'
                , ['country_name', 'country_iso', 'country_member_eu', 'country_currency_euro']
                , Select::JOIN_LEFT);
            $select->where(['address_supplier_rel_uuid' => $addressSupplierRelUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getAddressSupplierRelForAddressAndSupplier(string $addressUuid, string $supplierUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['address_uuid' => $addressUuid, 'supplier_uuid' => $supplierUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function existAddressSupplierRelForAddressAndSupplier(string $addressUuid, string $supplierUuid): bool
    {
        $address = $this->getAddressSupplierRelForAddressAndSupplier($addressUuid, $supplierUuid);
        return !empty($address) && is_array($address);
    }

    /**
     * @param string $supplierUuid
     * @return array
     */
    public function getAddressesForSupplier(string $supplierUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->join('address', 'address.address_uuid = address_supplier_rel.address_uuid'
                , $this->addressColumns
                , Select::JOIN_LEFT);
            $select->join('country', 'country.country_id = address.country_id'
                , ['country_name', 'country_iso', 'country_member_eu', 'country_currency_euro']
                , Select::JOIN_LEFT);
            $select->where(['supplier_uuid' => $supplierUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    protected function insertAddressSupplierRel(string $addressUuid, string $supplierUuid): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'address_supplier_rel_uuid' => $uuid,
                'address_uuid' => $addressUuid,
                'supplier_uuid' => $supplierUuid,
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function insertAddress(AddressEntity $addressEntity, string $supplierUuid): string
    {
        /** @var AbstractConnection $connection */
        $connection = $this->adapter->getDriver()->getConnection();
        $connection->beginTransaction();

        if (
            empty($addressUuid = $this->addressTable->insertAddress($addressEntity))
            || empty($addressSupplierRelUuid = $this->insertAddressSupplierRel($addressUuid, $supplierUuid))
        ) {
            $connection->rollback();
            return '';
        }
        $connection->commit();
        return $addressSupplierRelUuid;
    }

    /**
     * @param string $addressSupplierRelUuid
     * @return bool
     */
    public function deleteAddress(string $addressSupplierRelUuid): bool
    {
        $addressrel = $this->getAddressSupplierRel($addressSupplierRelUuid);
        if (empty($addressrel) || !is_array($addressrel)) {
            return false;
        }

        /** @var AbstractConnection $connection */
        $connection = $this->adapter->getDriver()->getConnection();
        $connection->beginTransaction();

        $delete = $this->sql->delete();
        try {
            $delete->where(['address_supplier_rel_uuid' => $addressSupplierRelUuid]);
            if ($this->executeDelete($delete) < 1) {
                $connection->rollback();
                return false;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }

        if ($this->addressTable->deleteAddress($addressrel['address_uuid']) < 1) {
            $connection->rollback();
            return false;
        }

        $connection->commit();
        return true;
    }

    public function updateAddress(AddressEntity $addressEntity, string $supplierUuid): int
    {
        $addressUuid = $addressEntity->getAddressUuid();
        if(!$this->existAddressSupplierRelForAddressAndSupplier($addressUuid, $supplierUuid)) {
            return -1;
        }
        $addressEntity->unsetPrimaryKey();
        return $this->addressTable->updateAddress($addressUuid, $addressEntity);
    }
}

<?php

namespace Lerp\Supplier\Table\Bank;

use Bitkorn\Bank\Entity\BankEntity;
use Bitkorn\Bank\Table\AbstractBankTable;
use Bitkorn\Bank\Table\BankTable;
use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\Adapter\Driver\AbstractConnection;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class BankSupplierRelTable extends AbstractBankTable
{
    /** @var string */
    protected $table = 'bank_supplier_rel';

    /**
     * @param string $bankSupplierRelUuid
     * @return array
     */
    public function getBankSupplierRel(string $bankSupplierRelUuid)
    {
        $select = $this->sql->select();
        try {
            $select->where(['bank_supplier_rel_uuid' => $bankSupplierRelUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $bankSupplierRelUuid
     * @return array
     */
    public function getBankSupplierRelJoined(string $bankSupplierRelUuid)
    {
        $select = $this->sql->select();
        try {
            $select->join('bank', 'bank.bank_uuid = bank_supplier_rel.bank_uuid'
                , $this->bankColumns
                , Select::JOIN_LEFT);
            $select->where(['bank_supplier_rel_uuid' => $bankSupplierRelUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getBankSupplierRelForBankAndSupplier(string $bankUuid, string $supplierUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['bank_uuid' => $bankUuid, 'supplier_uuid' => $supplierUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function existBankSupplierRelForBankAndSupplier(string $bankUuid, string $supplierUuid): bool
    {
        $bank = $this->getBankSupplierRelForBankAndSupplier($bankUuid, $supplierUuid);
        return !empty($bank) && is_array($bank);
    }

    /**
     * @param string $supplierUuid
     * @return array
     */
    public function getBanksForSupplier(string $supplierUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->join('bank', 'bank.bank_uuid = bank_supplier_rel.bank_uuid'
                , $this->bankColumns
                , Select::JOIN_LEFT);
            $select->where(['supplier_uuid' => $supplierUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    protected function insertBankSupplierRel(string $bankUuid, string $supplierUuid): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'bank_supplier_rel_uuid' => $uuid,
                'bank_uuid' => $bankUuid,
                'supplier_uuid' => $supplierUuid,
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function insertBank(BankEntity $bankEntity, string $supplierUuid): string
    {
        /** @var AbstractConnection $connection */
        $connection = $this->adapter->getDriver()->getConnection();
        $connection->beginTransaction();

        if (
            empty($bankUuid = $this->bankTable->insertBank($bankEntity))
            || empty($bankSupplierRelUuid = $this->insertBankSupplierRel($bankUuid, $supplierUuid))
        ) {
            $connection->rollback();
            return '';
        }
        $connection->commit();
        return $bankSupplierRelUuid;
    }

    /**
     * @param string $bankSupplierRelUuid
     * @return bool
     */
    public function deleteBank(string $bankSupplierRelUuid): bool
    {
        $bankrel = $this->getBankSupplierRel($bankSupplierRelUuid);
        if (empty($bankrel) || !is_array($bankrel)) {
            return false;
        }

        /** @var AbstractConnection $connection */
        $connection = $this->adapter->getDriver()->getConnection();
        $connection->beginTransaction();

        $delete = $this->sql->delete();
        try {
            $delete->where(['bank_supplier_rel_uuid' => $bankSupplierRelUuid]);
            if ($this->executeDelete($delete) < 1) {
                $connection->rollback();
                return false;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }

        if ($this->bankTable->deleteBank($bankrel['bank_uuid']) < 1) {
            $connection->rollback();
            return false;
        }

        $connection->commit();
        return true;
    }

    public function updateBank(BankEntity $bankEntity, string $supplierUuid): int
    {
        $bankUuid = $bankEntity->getBankUuid();
        if (!$this->existBankSupplierRelForBankAndSupplier($bankUuid, $supplierUuid)) {
            return -1;
        }
        $bankEntity->unsetPrimaryKey();
        return $this->bankTable->updateBank($bankUuid, $bankEntity);
    }
}

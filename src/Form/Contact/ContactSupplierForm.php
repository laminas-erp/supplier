<?php

namespace Lerp\Supplier\Form\Contact;

use Bitkorn\Contact\Form\Fieldset\ContactFieldset;
use Bitkorn\Trinket\Form\AbstractForm;
use Laminas\InputFilter\InputFilterProviderInterface;

class ContactSupplierForm extends AbstractForm implements InputFilterProviderInterface
{
    protected ContactFieldset $contactFieldset;

    public function setContactFieldset(ContactFieldset $contactFieldset): void
    {
        $this->contactFieldset = $contactFieldset;
    }

    public function getContactFieldset(): ContactFieldset
    {
        return $this->contactFieldset;
    }

	public function init()
	{
		$this->add($this->contactFieldset);
	}

	/**
	 * Should return an array specification compatible with
	 * {@link \Laminas\InputFilter\Factory::createInputFilter()}.
	 * @return array
	 */
	public function getInputFilterSpecification()
	{
		return $this->contactFieldset->getInputFilterSpecification();
	}
}

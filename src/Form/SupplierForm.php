<?php

namespace Lerp\Supplier\Form;

use Bitkorn\Trinket\Form\AbstractForm;
use Laminas\Db\Adapter\Adapter;
use Laminas\Filter\HtmlEntities;
use Laminas\Filter\StringTrim;
use Laminas\Filter\StripTags;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Validator\Db\RecordExists;
use Laminas\Validator\Digits;
use Laminas\Validator\EmailAddress;
use Laminas\Validator\InArray;
use Laminas\Validator\StringLength;
use Laminas\Validator\Uuid;

class SupplierForm extends AbstractForm implements InputFilterProviderInterface
{

    /**
     * @var array
     */
    protected $supportedLangIsos = [];

    /**
     * @var Adapter
     */
    protected $adapter;

    /**
     * @param array $supportedLangIsos
     */
    public function setSupportedLangIsos(array $supportedLangIsos): void
    {
        $this->supportedLangIsos = $supportedLangIsos;
    }

    /**
     * @param Adapter $adapter
     */
    public function setAdapter(Adapter $adapter): void
    {
        $this->adapter = $adapter;
    }

    public function init()
    {
        if ($this->primaryKeyAvailable) {
            $this->add(['name' => 'supplier_uuid']);
        }
//        $this->add(['name' => 'supplier_no']); // auto generated and never updated
        $this->add(['name' => 'supplier_name']);
        $this->add(['name' => 'supplier_name2']);
        $this->add(['name' => 'supplier_street']);
        $this->add(['name' => 'supplier_zip']);
        $this->add(['name' => 'supplier_city']);
        $this->add(['name' => 'country_id']);
        $this->add(['name' => 'supplier_tel']);
        $this->add(['name' => 'supplier_fax']);
        $this->add(['name' => 'supplier_email']);
        $this->add(['name' => 'supplier_www']);
        $this->add(['name' => 'supplier_lang_iso']);
        $this->add(['name' => 'industry_category_uuid']);
        $this->add(['name' => 'supplier_tax_id']);
        $this->add(['name' => 'supplier_note']);
    }

    /**
     * Should return an array specification compatible with
     * {@link \Laminas\InputFilter\Factory::createInputFilter()}.
     * @return array
     */
    public function getInputFilterSpecification()
    {
        $filter = [];

        if ($this->primaryKeyAvailable) {
            $filter['supplier_uuid'] = [
                'required' => true,
                'filters' => [
                    ['name' => StringTrim::class],
                    ['name' => HtmlEntities::class],
                    ['name' => StripTags::class]
                ],
                'validators' => [
                    ['name' => Uuid::class]
                ]
            ];
        }

//        $filter['supplier_no'] = [
//            'required' => true,
//            'filters' => [
//                ['name' => StringTrim::class],
//                ['name' => HtmlEntities::class],
//                ['name' => StripTags::class]
//            ],
//            'validators' => [
//                ['name' => Digits::class]
//            ]
//        ];

        $filter['supplier_name'] = [
            'required' => true,
            'filters' => [
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
                ['name' => StripTags::class]
            ],
            'validators' => [
                ['name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 3,
                        'max' => 180,
                    ]
                ]
            ]
        ];

        $filter['supplier_name2'] = [
            'required' => false,
            'filters' => [
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
                ['name' => StripTags::class]
            ],
            'validators' => [
                ['name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 3,
                        'max' => 180,
                    ]
                ]
            ]
        ];

        $filter['supplier_street'] = [
            'required' => true,
            'filters' => [
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
                ['name' => StripTags::class]
            ],
            'validators' => [
                ['name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 3,
                        'max' => 180,
                    ]
                ]
            ]
        ];

        $filter['supplier_zip'] = [
            'required' => true,
            'filters' => [
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
                ['name' => StripTags::class]
            ],
            'validators' => [
                ['name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 3,
                        'max' => 10,
                    ]
                ]
            ]
        ];

        $filter['supplier_city'] = [
            'required' => true,
            'filters' => [
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
                ['name' => StripTags::class]
            ],
            'validators' => [
                ['name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 3,
                        'max' => 180,
                    ]
                ]
            ]
        ];

        $filter['country_id'] = [
            'required' => true,
            'filters' => [
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
                ['name' => StripTags::class]
            ],
            'validators' => [
                ['name' => RecordExists::class,
                    'options' => [
                        'adapter' => $this->adapter,
                        'table' => 'country',
                        'field' => 'country_id',
                    ]
                ]
            ]
        ];

        $filter['supplier_tel'] = [
            'required' => false,
            'filters' => [
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
                ['name' => StripTags::class]
            ],
            'validators' => [
                ['name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 3,
                        'max' => 80,
                    ]
                ]
            ]
        ];

        $filter['supplier_fax'] = [
            'required' => false,
            'filters' => [
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
                ['name' => StripTags::class]
            ],
            'validators' => [
                ['name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 3,
                        'max' => 80,
                    ]
                ]
            ]
        ];

        $filter['supplier_email'] = [
            'required' => true,
            'filters' => [
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
                ['name' => StripTags::class]
            ],
            'validators' => [
                ['name' => EmailAddress::class]
            ]
        ];

        $filter['supplier_www'] = [
            'required' => false,
            'filters' => [
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
                ['name' => StripTags::class]
            ],
            'validators' => [
                ['name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 3,
                        'max' => 180,
                    ]
                ]
            ]
        ];

        $filter['supplier_lang_iso'] = [
            'required' => true,
            'filters' => [
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
                ['name' => StripTags::class]
            ],
            'validators' => [
                [
                    'name' => InArray::class,
                    'options' => [
                        'haystack' => $this->supportedLangIsos
                    ]
                ]
            ]
        ];

        $filter['industry_category_uuid'] = [
            'required' => true,
            'filters' => [
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
                ['name' => StripTags::class]
            ],
            'validators' => [
                ['name' => RecordExists::class,
                    'options' => [
                        'adapter' => $this->adapter,
                        'table' => 'industry_category',
                        'field' => 'industry_category_uuid',
                    ]
                ]
            ]
        ];

        $filter['supplier_tax_id'] = [
            'required' => false,
            'filters' => [
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
                ['name' => StripTags::class]
            ],
            'validators' => [
                ['name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 3,
                        'max' => 20,
                    ]
                ]
            ]
        ];

        $filter['supplier_note'] = [
            'required' => false,
            'filters' => [
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
                ['name' => StripTags::class]
            ],
            'validators' => [
                ['name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 0,
                        'max' => 64000,
                    ]
                ]
            ]
        ];

        return $filter;
    }
}

<?php

namespace Lerp\Supplier\Form\Bank;

use Bitkorn\Bank\Form\Fieldset\BankFieldset;
use Bitkorn\Trinket\Form\AbstractForm;
use Laminas\Filter\HtmlEntities;
use Laminas\Filter\StringTrim;
use Laminas\Filter\StripTags;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Validator\StringLength;

class BankSupplierForm extends AbstractForm implements InputFilterProviderInterface
{
    protected BankFieldset $bankFieldset;

    public function setBankFieldset(BankFieldset $bankFieldset): void
    {
        $this->bankFieldset = $bankFieldset;
    }

    public function getBankFieldset(): BankFieldset
    {
        return $this->bankFieldset;
    }

    public function init()
    {
        $this->add($this->bankFieldset);
    }

    /**
     * Should return an array specification compatible with
     * {@link \Laminas\InputFilter\Factory::createInputFilter()}.
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return $this->bankFieldset->getInputFilterSpecification();
    }
}
